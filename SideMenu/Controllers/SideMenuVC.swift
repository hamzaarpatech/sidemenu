//
//  SideMenuTableViewController.swift
//  SideMenu
//
//  Created by Hamza on 1/31/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SideMenuVC: UIViewController {
    
    var menuData:[SideMenuItem] = []
    @IBOutlet weak var sideVcCloseView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        populateData()
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(backgroundTapped))
        self.sideVcCloseView.addGestureRecognizer(tapGesture)
    }
        
    @objc func backgroundTapped() {
        let notifier = Notification.Name(rawValue: backgroundTapNotifier)
        NotificationCenter.default.post(name: notifier, object: nil)
    }

    @objc func removeView() {
      self.view.removeFromSuperview()
    }
    
    func populateData(){
        menuData = [
            SideMenuItem(title: "Meals", image: #imageLiteral(resourceName: "meals"), controller: "MealsVC") ,
            SideMenuItem(title: "Order History", image: #imageLiteral(resourceName: "order-history"), controller: "OrderHistoryVC") ,
            SideMenuItem(title: "Donate a Meal", image: #imageLiteral(resourceName: "donat a meal"), controller: "DonateMealVC") ,
            SideMenuItem(title: "LeaderBoard", image: #imageLiteral(resourceName: "leaderboard"), controller: "LeaderBoardVC") ,
            SideMenuItem(title: "Help", image: #imageLiteral(resourceName: "help"), controller: "HelpVC") ,
            SideMenuItem(title: "FAQs", image: #imageLiteral(resourceName: "faqs"), controller: "FaqVC") ,
            SideMenuItem(title: "Contact us", image: #imageLiteral(resourceName: "contact-us"), controller: "ContactUsVC") ,
            SideMenuItem(title: "Suggest a Meal", image: #imageLiteral(resourceName: "suggest-a-meal"), controller: "SuggestMealVC") ,
            SideMenuItem(title: "Invite Friends", image: #imageLiteral(resourceName: "invite-friends"), controller: "InviteFriendsVC") ,
            SideMenuItem(title: "Logout", image: #imageLiteral(resourceName: "logout"), controller: nil)
        ]
    }
    
    func onItemSelect() {
         let notifier = Notification.Name(rawValue: backgroundTapNotifier)
         NotificationCenter.default.post(name: notifier, object: menuData.self)
    }
}

extension SideMenuVC : UITableViewDelegate , UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return menuData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? SideMenuTableViewCell
        cell?.model = menuData[indexPath.row]
        return cell!
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let data =  menuData[indexPath.row]
        let notifier = Notification.Name(rawValue: itemSelectNotifier)
        NotificationCenter.default.post(name: notifier, object: data, userInfo: ["values" : data])
   }
}
