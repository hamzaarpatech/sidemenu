//
//  MainVC.swift
//  SideMenu
//
//  Created by Hamza on 1/31/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

let menuButtonBack = "com.menubutton.tapped"
let backListener = Notification.Name(rawValue: menuButtonBack)
let backgroundTapNotifier = "com.sideview.tapped"
let backListener2 = Notification.Name(rawValue: backgroundTapNotifier)
let itemSelectNotifier = "com.itemselected.tapped"
let backListener3 = Notification.Name(rawValue: itemSelectNotifier)

class MainVC: UIViewController  {
    
    var sideMenu: SideMenuVC?
    var selectedContentMenu: UIView!
    var sideMenuTrailing: NSLayoutConstraint!
    @IBOutlet weak var passMenuLabel: UILabel!
    
    deinit{
        NotificationCenter.default.removeObserver(self)
    }
  
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
   @objc func updateMyView(notification : NSNotification){
         self.showMenu()
    }
    
    @objc func backgroundTap(notification : NSNotification){
        self.hideMenu()
    }
    
    @objc func itemSelected(notification : NSNotification) -> Void {
        guard let passItemData = notification.userInfo?["values"] else { return }
        handleMenuSelect(item: passItemData as! SideMenuItem)
    }
    
    @IBAction func toggleMenu(sender: UIButton) {
        self.callMenu()
    }
    
    func callMenu() {
        if sideMenu == nil {
            if let vc = storyboard?.instantiateViewController(identifier: "SideMenuVC") as? SideMenuVC {
                sideMenu = vc
                NotificationCenter.default.addObserver(self, selector: #selector(MainVC.backgroundTap(notification:)), name: backListener2, object: nil)
                NotificationCenter.default.addObserver(self, selector: #selector(MainVC.itemSelected(notification:)), name: backListener3, object: nil)
                self.addChild(sideMenu!)
           }
            let menuView = sideMenu?.view
            if menuView?.superview == nil {
               showMenu()
            }
       }
    }
    
    func showMenu() {
        let menuView = sideMenu?.view
        self.view.addSubview(menuView!)
        menuView?.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate([
            menuView!.topAnchor.constraint(equalTo: self.view.topAnchor),
            menuView!.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            menuView!.widthAnchor.constraint(equalTo: self.view.widthAnchor)
        ])
        
        sideMenuTrailing = menuView!.trailingAnchor.constraint(equalTo: self.view.leadingAnchor)
        sideMenuTrailing.isActive = true
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.sideMenuTrailing.constant = self.view.bounds.width
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }, completion: nil)
    }
    
    func hideMenu() {
        let menuView = sideMenu?.view
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 0.9, initialSpringVelocity: 0, options: .curveEaseOut, animations: {
            self.sideMenuTrailing.constant = 0
            self.view.setNeedsLayout()
            self.view.layoutIfNeeded()
        }) { _ in
            menuView?.removeFromSuperview()
        }
    }
    
    func handleMenuSelect(item: SideMenuItem) {
        passMenuLabel.text = item.title
        self.hideMenu()
        
        if let identifier = item.viewController {
            addVC(with: identifier, item: item)
        }
    }
    
    func addVC(with identifier: String, item: SideMenuItem) {
        if selectedContentMenu != nil {
            selectedContentMenu.removeFromSuperview()
        }
        
        let vc = storyboard?.instantiateViewController(withIdentifier: identifier) as! SideMenuContentVC
        selectedContentMenu = vc.view!
        selectedContentMenu.translatesAutoresizingMaskIntoConstraints = false
        vc.menuItem = item
        
        self.addChild(vc)
        self.view.insertSubview(selectedContentMenu, belowSubview: sideMenu!.view)
        
        NotificationCenter.default.addObserver(self, selector: #selector(MainVC.updateMyView(notification:)), name: backListener, object: nil)
        NSLayoutConstraint.activate([
            selectedContentMenu.leadingAnchor.constraint(equalTo: self.view.leadingAnchor),
            selectedContentMenu.trailingAnchor.constraint(equalTo: self.view.trailingAnchor),
            selectedContentMenu.bottomAnchor.constraint(equalTo: self.view.bottomAnchor),
            selectedContentMenu.topAnchor.constraint(equalTo: self.view.topAnchor)
        ])
        self.view.setNeedsLayout()
        self.view.layoutIfNeeded()
    }
}
