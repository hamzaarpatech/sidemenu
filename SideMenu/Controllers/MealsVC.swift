//
//  MealsVC.swift
//  SideMenu
//
//  Created by Hamza on 2/7/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class MealsVC: SideMenuContentVC {
   
    @IBOutlet weak var menuTitle: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let item = self.menuItem {
            menuTitle.text = item.title
            print(item.title)
        }
    }

    @IBAction func toggleMenu(_ sender: UIButton) {
       openSideMenu()
    }
}
