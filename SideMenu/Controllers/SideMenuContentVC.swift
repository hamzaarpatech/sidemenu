//
//  SideMenuContentVC.swift
//  SideMenu
//
//  Created by Faizan Naseem on 10/02/2020.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SideMenuContentVC: UIViewController {

    var menuItem: SideMenuItem?
    override func viewDidLoad() {
          super.viewDidLoad()
      }
 
    func openSideMenu() {
        let notifier = Notification.Name(rawValue: menuButtonBack)
        NotificationCenter.default.post(name: notifier, object: nil)
    }
}
