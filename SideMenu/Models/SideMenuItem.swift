//
//  SideMenuItem.swift
//  SideMenu
//
//  Created by Hamza on 2/7/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import Foundation
import UIKit

class SideMenuItem {
    let title : String
    let image : UIImage
    let viewController: String?
     
    init(title: String , image: UIImage, controller: String?) {
        self.title = title
        self.image = image
        self.viewController = controller
    }
}
