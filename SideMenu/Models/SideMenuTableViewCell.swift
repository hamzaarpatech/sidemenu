//
//  SideMenuTableViewCell.swift
//  SideMenu
//
//  Created by Hamza on 1/31/20.
//  Copyright © 2020 Hamza. All rights reserved.
//

import UIKit

class SideMenuTableViewCell: UITableViewCell {

    @IBOutlet weak var menuItems: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    
    var model : SideMenuItem! {
        didSet {
            menuItems.text = model.title
            imgView.image = model.image
        }
    }
}
